from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .apps import Story7Config

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
class Story7UnitTest(TestCase):

    def test_if_url_exist_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
        
    def test_if_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_if_page_does_not_exist(self):
        response = Client().get('wek/')
        self.assertEqual(response.status_code, 404)
        
    def test_if_index_used_the_right_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')


class FunctionalTest(StaticLiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.headless = True
        
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(LandingPageFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_form(self):
        selenium = self.selenium

        # Opening the link we want to test
        selenium.get('http://localhost:8000/')
        time.sleep(5)

        clickit = selenium.find_element_by_class('mylab')
        clickit.send_keys(Keys.RETURN)
        time.sleep(10)

